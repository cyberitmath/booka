package com.parto.shayan.bookstore;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import com.parto.shayan.bookstore.adapter.menuAdapter;

public class MenuGridViewActivity extends AppCompatActivity {
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_grid_view);
        gridView = (GridView) findViewById(R.id.gridView);
        String icons[] = {
                "@drawable/library",
                "@drawable/store",
                "@drawable/weather",
                "@drawable/call"
        };
        String nameIcons[] = {
                getString(R.string.myLibrary),
                getString(R.string.store),
                getString(R.string.weather),
                getString(R.string.tell)
        };



        menuAdapter adapter = new menuAdapter(this, icons, nameIcons);
        gridView.setAdapter(adapter);

        
    }

}
