package com.parto.shayan.bookstore.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parto.shayan.bookstore.MenuGridViewActivity;
import com.parto.shayan.bookstore.R;

/**
 * Created by shayan on 11/15/2017.
 */

public class menuAdapter extends BaseAdapter {
    Context mContext;
    String icons[];
    String  nameIcons[];

    public menuAdapter(MenuGridViewActivity menuGridViewActivity, String[] icons, String[] nameIcons){
        this.mContext=mContext;
        this.icons= this.icons;
        this.nameIcons= this.nameIcons;

    }


    @Override
    public int getCount() {
        return nameIcons.length;
    }

    @Override
    public Object getItem(int position) {
        return nameIcons[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.menu_list_name, viewGroup, false);
        ImageView iconAvatar1=(ImageView)rowView.findViewById(R.id.iconAvatar1);
        iconAvatar1.setImageDrawable(Drawable.createFromPath(icons[position]));
        TextView menuName1=(TextView)rowView.findViewById(R.id.menuName1);
        menuName1.setText(nameIcons[position]);
        return rowView;

    }
}
